﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task_2
{
    public partial class MainPage : ContentPage
    {
        public static int correctGuesses = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        void Entry_Completed(Object sender, System.EventArgs e)
        {
            var userNumber = new userNumber();
            Random i = new Random();
            int randInt = i.Next(1, 10);
            var value = 0;

            if (int.TryParse(userEntry.Text, out value))
            {
                if (userNumber.userGuess == randInt)
                {
                    result.Text = "Correct!";
                    Background.BackgroundColor = Color.Green;
                    correctGuesses++;
                }
                else
                {
                    result.Text = "Wrong, Sorry";
                    Background.BackgroundColor = Color.Red;
                }
                guesses.Text = $"You have guessed correctly {correctGuesses} times!";
            }
            else
            {
                guesses.Text = "Please enter a valid number";
            }
        }
    }



}
