﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assessment_1_Task_1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Background2.BackgroundColor = Color.FromHex("87ceeb");
            Background.BackgroundColor = Color.Aqua;
        }
        
        void Entry_Completed (Object sender, System.EventArgs e)
        {
            var userNumber = new userNumber();
            var i = 0;
            var sum = 0;
            var input = 0;

            if (int.TryParse(userEntry.Text, out input))
            {
                userNumber.number = input;
                for (i = 0; i < userNumber.number; i++)
                {
                    if ((i % 3) == 0 || (i % 5) == 0)
                    {
                        sum = sum + i;
                    }
                }
                total.Text = $"The sum of the multiples of 3 and 5 that are less than {userNumber.number} is {sum}";
            }
            else
            {
                total.Text = "Please enter a valid number.";
            }
                

            
        }
    }
}
